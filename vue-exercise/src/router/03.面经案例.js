import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'
import Longin from '../mianRouter/Longin.vue'
import Dateiil from '../mianRouter/Dateiil.vue'
import Shouye from '../mianRouter/sub/Shouye.vue'
import Shoucan from '../mianRouter/sub/Shoucan.vue'
import Love from '../mianRouter/sub/Love.vue'
import My from '../mianRouter/sub/My.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/dateiil/:id',
    name: 'Dateiil',
    component: Dateiil,
  },
  {
    path: '/',
    name: 'Longin',
    component: Longin,
    redirect: '/',
    children: [
      {
        path: '/',
        name: 'Shouye',
        component: Shouye,
      },
      {
        path: '/shoucan',
        name: 'Shoucan',
        component: Shoucan,
      },
      {
        path: '/love',
        name: 'Love',
        component: Love,
      },
      {
        path: '/my',
        name: 'My',
        component: My,
      },

    ]
  },

]

const router = new VueRouter({
  mode: 'hash',
  routes
})

export default router
