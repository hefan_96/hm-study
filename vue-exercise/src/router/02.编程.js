import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'
import One from '../routerValue/One.vue'
import Two from '../routerValue/Two.vue'
import Threrr from '../routerValue/Threrr.vue'


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'one',
    component: One
  },
  {
    path: '/two',
    name: 'two',
    component: Two
  },
  {
    path: '/threrr/:age',
    name: 'threrr',
    component: Threrr
  },
]

const router = new VueRouter({
  mode: 'hash',
  routes
})

export default router
