import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'
import One from '../vueRouter/One.vue'
import Two from '../vueRouter/Two.vue'
import Three from '../vueRouter/Three.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'One',
    component: One
  },
  {
    path: '/Two',
    name: 'Two',
    component: Two
  },
  {
    path: '/Three',
    name: 'Three',
    component: Three
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
