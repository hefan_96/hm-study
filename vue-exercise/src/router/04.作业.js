import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'

import Login from '../job/Login.vue'
import Home from '../job/Home.vue'
import First from '../job/sel/First.vue'
import Second from '../job/sel/Second.vue'
import Third from '../job/sel/Third.vue'
import Forth from '../job/sel/Forth.vue'
import Xinyi from '../mianRouter/sub/three/Xinyi.vue'
import Xiner from '../mianRouter/sub/three/Xiner.vue'
import Xinsan from '../mianRouter/sub/three/Xinsan.vue'



Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Login',
    component: Login,
  },
  {
    path: '/home',
    name: 'Home',
    component: Home,
    redirect: '/first',
    children: [
      {
        path: '/first',
        name: 'First',
        component: First,
        redirect: '/xinyi',
        children: [
          {
            path: '/xinyi',
            name: 'Xinyi',
            component: Xinyi,
          },
          {
            path: '/xiner',
            name: 'Xiner',
            component: Xiner,
          },
          {
            path: '/xinsan',
            name: 'Xinsan',
            component: Xinsan,
          },
        ]
      },
      {
        path: '/second',
        name: 'Second',
        component: Second,
      },
      {
        path: '/third/:third',
        name: 'Third',
        component: Third,
      },
      {
        path: '/forth/:forth',
        name: 'Forth',
        component: Forth,
      },
    ]
  },
]

const router = new VueRouter({
  mode: 'hash',
  routes
})

export default router
