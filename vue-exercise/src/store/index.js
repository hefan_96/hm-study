import Vue from 'vue'
import Vuex from 'vuex'
import user from './modules/user'


Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    //  定义数据
    count: 20,
    obj: {
      name: 'hefan',
      age: 20,
    },
    list: [1, 2, 3, 4, 5, 8, 9]
  },
  getters: {
    //   计算 相当于computed
    toalb (state) {
      return state.list.reduce((a, b) => a + b, 0)
    }
  },
  mutations: {
    //   修改数据:state里面的数据只能在mutations里面修改
    //   同步操作
    numDate (state) {
      state.count += 11
    },
    getDate (state, t) {
      state.obj.name = t
    },
    getAction (state, a) {
      this.state.obj.age += a
    }
  },
  actions: {
    //   异步操作
    getActions (b, n) {
      setTimeout(() => {
        b.commit('getAction', n)
      }, 1000)
    },

  },
  modules: {
    //  模块
    user
  }
})
