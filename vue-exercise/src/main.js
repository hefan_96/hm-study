import Vue from 'vue'
import App from './APP.vue'
import router from './router'
import store from './store'
import pluginObj from '@/components'
//   全部引入
import Vant from 'vant';
import 'vant/lib/index.css';
// 把vant中所有的组件都导入了
Vue.use(Vant)
Vue.use(pluginObj)
Vue.config.productionTip = false
//   自定义指令全局
Vue.directive('focus', {
  //   钩子函数
  //  inserted(dom元素(el),传的值(binding))
  inserted (el, binding) {
    //  执行时机:使用这个指令的dom加载完成就执行这个钩子函数
    el.focus()
  }
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
