import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
//  导入公共样式
import '@/styles/index.scss'
//  导入element组件
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
// 富文本全局引入
import VueQuillEditor from 'vue-quill-editor'

// require styles
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'
Vue.use(ElementUI)
Vue.config.productionTip = false

Vue.use(VueQuillEditor /* { default global options } */)
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
