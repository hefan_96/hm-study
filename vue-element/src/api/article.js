import request from '@/utils/request'
export const ArticleApi = (data) => {
  return request.get('/admin/interview/query', {
    params: data
  })
}
//   删除
export const delArticle = id => {
  return request.delete('/admin/interview/remove', {
    data: {
      id
    }
  })
}
//  添加文章
export const addArticle = (data) => {
  return request.post('/admin/interview/create', data)
}
//   修改面经
export const getArticle = (data) => {
  return request.put('/admin/interview/update', data)
}
