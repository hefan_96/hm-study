import request from '@/utils/request'
//  登陆页面接口
export const loginApi = (data) => {
  return request.post('/auth/login', data)
}
//   首页接口
export const getUser = () => {
  return request.get('/auth/currentUser')
}
