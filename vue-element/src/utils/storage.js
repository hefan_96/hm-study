const KEY = 'vue-element-token'
//   取
export const getToken = () => {
  return localStorage.getItem(KEY)
}
//   存
export const setToken = (newToken) => {
  localStorage.setItem(KEY, newToken)
}
//   删除
export const delToken = () => {
  localStorage.removeItem(KEY)
}
