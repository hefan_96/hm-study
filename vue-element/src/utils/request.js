import axios from 'axios'
import router from '../router'
import { getToken, delToken } from '../utils/storage'
const request = axios.create({
  baseURL: 'http://interview-api-t.itheima.net/',
  timeout: 5000

})

// 添加请求拦截器
request.interceptors.request.use(
  function (config) {
    // 在发送请求之前做些什么
    const token = getToken()
    if (token) {
      config.headers.Authorization = 'Bearer ' + token
    }
    return config
  }, function (error) {
    // 对请求错误做些什么
    return Promise.reject(error)
  })

// 添加响应拦截器
request.interceptors.response.use(function (response) {
  // 对响应数据做点什么
  return response.data
}, function (error) {
  // 对响应错误做点什么
  console.log(error, 88888)
  if (error.response.data.code === 401) {
    delToken()
    router.push('/login')
  }

  return Promise.reject(error)
})
export default request
