import { loginApi } from '@/api/user'
import { getToken, setToken, delToken } from '@/utils/storage'
export default {
  state () {
    return {
      token: getToken()
    }
  },
  mutations: {
    logingChack (state, b) {
      state.token = b
      setToken(b)
    },
    tuichuToken (state) {
      state.token = null
      delToken()
    }
  },
  actions: {
    async loginGet (a, obj) {
      const res = await loginApi(obj)
      // console.log(res.data.data.token)
      a.commit('logingChack', res.data.token)
      // console.log(obj, 44444)
    }
  },
  getters: {},
  namespaced: true
}
