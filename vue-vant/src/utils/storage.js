const KEY = 'vue-vant-token'
//    取
export const getToken = () => {
  return localStorage.getItem(KEY)
}
//   存储
export const setToken = (newToken) => {
  localStorage.setItem(KEY, newToken)
}
//  删除
export const delToken = () => {
  localStorage.removeItem(KEY)
}
