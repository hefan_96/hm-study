//   批量注册插件
export default function (Vue) {
  const requireComponent = require.context('./', true, /\.vue$/)
  requireComponent.keys().forEach(item => {
    // console.log(item,9999);//获取的组建的路径
    //  console.log( requireComponent(item).default,2222);
    const defaultObj = requireComponent(item).default
    Vue.component(defaultObj.name, defaultObj)
  })
}
