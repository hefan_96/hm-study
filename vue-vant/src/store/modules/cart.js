import axios from 'axios'
export default {
  state () {
    return {
      list: []
    }
  },
  mutations: {
    chanckList (state, b) {
      state.list = b
      // console.log(b, 444555)
    },
    chanckCount (state, obj) {
      const arr = state.list.find(item => item.id === obj.id)
      arr.count = obj.count
    }

  },
  actions: {
    async getList (a) {
      const res = await axios.get('http://localhost:3000/cart')
      // console.log(res.data, 888999)
      a.commit('chanckList', res.data)
    },
    async getCount (c, obj) {
      await axios.patch('http://localhost:3000/cart/' + obj.id, {
        count: obj.count
      })
      c.commit('chanckCount', obj)
    }
  },

  getters: {
    num1 (state) {
      return state.list.reduce((a, b) => a + b.count, 0)
    },
    num2 (state) {
      return state.list.reduce((a, b) => a + b.count * b.price, 0)
    }
  },
  namespaced: true
}
