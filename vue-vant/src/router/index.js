
import Vue from 'vue'
import VueRouter from 'vue-router'
import { getToken } from '../utils/storage'
// import HomeView from '../views/HomeView.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/Login',
    name: 'Login',
    component: () => import('../views/Login.vue')
  },
  {
    path: '/register',
    name: 'Register',
    component: () => import('../views/Register.vue')
  },
  {
    path: '/detail/:id',
    name: 'Detail',
    component: () => import('../views/Detail.vue')
  },
  {
    path: '/',
    name: 'Layout',
    redirect: '/article',
    component: () => import('../views/Layout.vue'),
    children: [
      {
        path: '/article',
        name: 'Article',
        component: () => import('../views/Article.vue')
      },
      {
        path: '/collect',
        name: 'Collect',
        component: () => import('../views/Collect.vue')
      },
      {
        path: '/like',
        name: 'Like',
        component: () => import('../views/Like.vue')
      },
      {
        path: '/my',
        name: 'My',
        component: () => import('../views/My.vue')
      }
    ]
  }
  // {
  //   path: '/',
  //   name: 'home',
  //   component: HomeView
  // },
  // {
  //   path: '/about',
  //   name: 'about',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
  // }
]

const router = new VueRouter({
  mode: 'hash',
  routes
})
const arr = ['/register', '/Login']
// console.log(router, 8521548)
router.beforeEach((to, from, next) => {
  const token = getToken()
  //    如果有token值则放行
  if (token) {
    next() //  放行
  } else {
    //  如果没有token,则在白名单里面找,找到了就放行
    if (arr.includes(to.path)) {
      next() //  放行
    } else {
      //   找不到就去登陆页
      next('/Login')
    }
  }
})
//   删除token后点击其他的页面报错解决方法
// 第1步: 保存原型对象的Push
const originPush = VueRouter.prototype.push
const originReplace = VueRouter.prototype.replace
// 第2步:重写push方法
VueRouter.prototype.push = function (location, res, rej) {
  if (res && rej) {
    originPush.call(this, location, res, rej)
  } else {
    originPush.call(
      this,
      location,
      () => { },
      () => { }
    )
  }
}
// 第3步:重写replace方法
VueRouter.prototype.replace = function (location, res, rej) {
  if (res && rej) {
    originReplace.call(this, location, res, rej)
  } else {
    originReplace.call(
      this,
      location,
      () => { },
      () => { }
    )
  }
}
export default router
