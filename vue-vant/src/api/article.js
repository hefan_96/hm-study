import request from '@/utils/request'
export const getArticles = (obj) => {
  return request.get('/interview/query', {
    params: {
      current: obj.current,
      pageSize: obj.pageSize,
      sorter: obj.sorter
    }
  })
}
