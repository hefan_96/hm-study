import request from '@/utils/request'

export const LikeCollectApi = (obj) => {
  return request.get('/interview/opt/list', {
    params: {
      optType: obj.optType,
      pageSize: obj.pageSize,
      page: obj.page
    }
  })
}
