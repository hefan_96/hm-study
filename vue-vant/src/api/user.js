import request from '@/utils/request'
//    登陆封装
export const login = (values) => {
  return request.post('/user/login', values)
}
//   注册封装
export const register = (values) => {
  return request.post('/user/register', values)
}
