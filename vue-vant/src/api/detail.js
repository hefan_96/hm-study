//    文章详情
import request from '@/utils/request'
export const detailApi = (id) => {
  return request.get('/interview/show', {
    params: {
      id
    }
  })
}
//  点赞和收藏
export const likeApi = (data) => {
  return request.post('/interview/opt', data)
}
