import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

//  组件全部导入
import Vant from 'vant'
import 'vant/lib/index.css'

import pluginobj from './components/index'
// 把vant中所有的组件都导入了
Vue.use(Vant)

Vue.use(pluginobj)
Vue.config.productionTip = false
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
